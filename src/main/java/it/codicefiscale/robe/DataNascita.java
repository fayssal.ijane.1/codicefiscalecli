package it.codicefiscale.robe;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

//◢◤
//I'll be waiting for love, waiting for love to come around
public class DataNascita {

	private String data;
	private char sesso;

	public void setDataNascita(LocalDate ld) {
		String anno = ld.format(DateTimeFormatter.ofPattern("yy"));
		char[] mesiLettera = { 'A', 'B', 'C', 'D', 'E', 'H', 'L', 'M', 'P', 'R', 'S', 'T' };
		char mese = mesiLettera[ld.getMonthValue() - 1];
		String giorno = "";
		if (sesso == 'M') {
			giorno = String.valueOf(ld.getDayOfMonth());
		} else {
			giorno = String.valueOf(ld.getDayOfMonth() + 40);
		}
		data = new StringBuilder(anno).append(mese).append(giorno).toString();
	}

	public void setDataNascita(String data) {
		this.data = data;
	}

	public char getSesso() {
		return sesso;
	}

	public void setSesso(char sesso) {
		this.sesso = sesso;
	}

	public String getDataNascita() {
		return this.data;
	}

}
