package it.codicefiscale.robe;

//◢◤
//All my life, I've been, I've been waiting for someone like you, yeah
public class Nominativo {
	private String nome;
	private String cognome;

	public String getNome() {
		return this.nome;
	}

	public String getCognome() {
		return this.cognome;
	}

	public void setCognome(String c) {
		String vocali = getVocali(c);
		String consonanti = getConsonanti(c);
		if (consonanti.length() > 3) {
			cognome = consonanti.substring(0, 3);
		} else if (consonanti.length() == 2) {
			cognome = (vocali.length() != 0) ? consonanti + vocali.charAt(0) : "errore";
		} else if (consonanti.length() == 1) {
			switch (vocali.length()) {
			case 1:
				cognome = consonanti + vocali.charAt(0) + 'X';
				break;
			case 0:
				cognome = "errore";
				break;
			default:
				cognome = consonanti.concat(vocali.substring(0, 2));
				break;
			}
		} else if (consonanti.length() == 3) {
			cognome = consonanti;
		} else {
			cognome = "errore";
		}
	}

	public void setNome(String n) {
		String vocali = getVocali(n);
		String consonanti = getConsonanti(n);
		if (consonanti.length() > 3) {
			nome = new StringBuilder(consonanti).deleteCharAt(1).substring(0, 3);
		} else if (consonanti.length() == 2) {
			nome = (vocali.length() != 0) ? consonanti + vocali.charAt(0) : "errore";
		} else if (consonanti.length() == 1) {
			switch (vocali.length()) {
			case 1:
				nome = consonanti + vocali.charAt(0) + 'X';
				break;
			case 0:
				nome = "errore";
				break;
			default:
				nome = consonanti.concat(vocali.substring(0, 2));
				break;
			}
		} else if (consonanti.length() == 3) {
			nome = consonanti;
		} else
			nome = "errore";
	}

	private String getConsonanti(String x) {
		return x.toUpperCase().replaceAll("[AEIOU]", "");
	}

	private String getVocali(String x) {
		return x.toUpperCase().replaceAll("[^AEIOU]", "");
	}
}
