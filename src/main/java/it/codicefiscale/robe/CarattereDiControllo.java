package it.codicefiscale.robe;

//◢◤
//"These are the nights that never die"
public class CarattereDiControllo {
	private String nome;
	private String data;
	private String codiceCatastale;

	public CarattereDiControllo(String nome, String data, String codiceCatastale) {
		super();
		this.nome = nome;
		this.data = data;
		this.codiceCatastale = codiceCatastale;
	}

	public char getCarattere() {
		String codice = nome.concat(data).concat(codiceCatastale);
		int temp;
		int sommaPari = 0, sommaDispari = 0, sommaTotale = 0;
		char carattere;
		int[] posizioneDispari = { 1, 0, 5, 7, 9, 13, 15, 17, 19, 21, 1, 0, 5, 7, 9, 13, 15, 17, 19, 21, 2, 4, 18, 20,
				11, 3, 6, 8, 12, 14, 16, 10, 22, 25, 24, 23 };
		char[] caratteriDiControllo = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F',
				'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z' };

		for (int i = 0; i < codice.length(); i++) {
			if (i % 2 != 0) {
				try {
					temp = Integer.valueOf(String.valueOf(codice.charAt(i)));
					sommaPari += temp;
				} catch (NumberFormatException ex) {
					sommaPari = sommaPari + (codice.charAt(i) - 'A');
				}
			} else {
				sommaDispari = sommaDispari
						+ posizioneDispari[String.valueOf(caratteriDiControllo).indexOf(codice.charAt(i))];
			}
		}
		sommaTotale = sommaPari + sommaDispari;
		temp = sommaTotale % 26;
		carattere = (char) (sommaTotale % 26 + 'A');
		return carattere;
	}
}
