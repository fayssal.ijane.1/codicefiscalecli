package it.codicefiscale.robe;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

//◢◤
//You said you'd follow me anywhere
//But your eyes tell me you won't be there
@XmlRootElement(name = "listaCodiciCatastali")
@XmlAccessorType(XmlAccessType.FIELD)
public class ListaCodiciCatastali {

	@XmlElement(name = "row")
	private List<CodiceCatastale> row;

	public ListaCodiciCatastali() {
		super();
	}

	public List<CodiceCatastale> getRow() {
		return row;
	}

	public void setRow(List<CodiceCatastale> row) {
		this.row = row;
	}

}
