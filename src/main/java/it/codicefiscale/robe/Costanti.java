package it.codicefiscale.robe;

//◢◤
//So wake me up when it's all over, when I'm wiser and I'm older
public enum Costanti {
	NOME("Inserisci nome valido:"), COGNOME("Inserisci cognome valido:"),
	DATA_NASCITA("Inserisci data di nascita (dd/mm/yyyy): "), SESSO("Inserisci sesso valido (M/F): "),
	COMUNE_NASCITA("Inserisci comune di nascita: "), MESSAGGIO_ERRORE_INSERIMENTO("Errore inserimento %s!\n"),
	MESSAGGIO_ERRORE_COLLEGAMENTO("Errore collegamento al %s: %s!\n"),
	MESSAGGIO_ATTENZIONE_ALTERNATIVA(
			"In assenza di un collegamento al %s, verrà effettuato un tentativo di lettura del codice dal %s\n"),
	USERNAME_DB("root"), PSW_DB("admin"), URL_DB("jdbc:mysql://127.0.0.1/codici_catastali"), DATABASE("Database"),
	XML("File XML"), JSON("File JSON"), MOTIVAZIONE_FILE("Il file non esiste, non è leggibile o è danneggiato!"),
	PERCORSO_JSON("src/main/resources/comuni.json"), PERCORSO_XML("src/main/resources/comuni.xml"),
	STAMPA_RISULTATO("Il tuo codice fiscale è %s");

	public final String label;

	private Costanti(String label) {
		this.label = label;
	}
}
