package it.codicefiscale.robe;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

//◢◤
//Oh, if the sky comes falling down, for you, there's nothing in this world I wouldn't do
@XmlRootElement(name = "row")
@XmlAccessorType(XmlAccessType.FIELD)
public class CodiceCatastale {

	private String codice;
	private String comune;

	public String getCodice() {
		return codice;
	}

	public void setCodice(String codice) {
		this.codice = codice;
	}

	public String getComune() {
		return comune;
	}

	public void setComune(String comune) {
		this.comune = comune;
	}
}
