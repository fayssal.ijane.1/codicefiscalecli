package it.codicefiscale;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Scanner;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import it.codicefiscale.robe.CarattereDiControllo;
import it.codicefiscale.robe.CodiceCatastale;
import it.codicefiscale.robe.Costanti;
import it.codicefiscale.robe.DataNascita;
import it.codicefiscale.robe.ListaCodiciCatastali;
import it.codicefiscale.robe.Nominativo;

//◢◤
//Oh, sometimes I get a good feeling, yeah
public class App {
	public static void main(String[] args) throws IOException, JAXBException {
		System.out.println("Benvenuto nel mio calcolatore di codici fiscali!");
		Scanner s = new Scanner(System.in);
		Nominativo n = new Nominativo();
		do {
			System.out.print(Costanti.NOME.label);
			n.setNome(s.nextLine());
			if (n.getNome().equalsIgnoreCase("errore")) {
				System.out.printf(Costanti.MESSAGGIO_ERRORE_INSERIMENTO.label, "nome");
			}
		} while (n.getNome().equalsIgnoreCase("errore"));

		do {
			System.out.print(Costanti.COGNOME.label);
			n.setCognome(s.nextLine());
			if (n.getNome().equalsIgnoreCase("errore")) {
				System.out.printf(Costanti.MESSAGGIO_ERRORE_INSERIMENTO.label, "cognome");
			}
		} while (n.getNome().equalsIgnoreCase("errore"));
		String nominativo = new StringBuilder(n.getCognome()).append(n.getNome()).toString();

		DataNascita dn = new DataNascita();

		do {
			System.out.print(Costanti.SESSO.label);
			char sesso = s.next().toUpperCase().charAt(0);
			if (sesso == 'M' || sesso == 'F') {
				dn.setSesso(sesso);
			} else {
				System.out.printf(Costanti.MESSAGGIO_ERRORE_INSERIMENTO.label, "sesso");
			}
		} while (dn.getSesso() != 'M' && dn.getSesso() != 'F');

		String data = "";
		do {
			System.out.print(Costanti.DATA_NASCITA.label);
			try {
				LocalDate ld = LocalDate.parse(s.nextLine(), DateTimeFormatter.ofPattern("dd/MM/yyyy"));
				if (ld.isAfter(LocalDate.now()))
					dn.setDataNascita("errore");
				else
					dn.setDataNascita(ld);
			} catch (DateTimeParseException ex) {
				dn.setDataNascita("errore");
				System.out.printf(Costanti.MESSAGGIO_ERRORE_INSERIMENTO.label, "data");
			}
		} while (dn.getDataNascita().equalsIgnoreCase("errore"));
		String formDataNascita = dn.getDataNascita();

		
		ObjectMapper om = new ObjectMapper();
		Connection con = null;
		List<CodiceCatastale> codici = new ArrayList<>();
		String codice="";
		System.out.print(Costanti.COMUNE_NASCITA.label);
		String comune = s.nextLine();
		try {
			con = DriverManager.getConnection(Costanti.URL_DB.label, Costanti.USERNAME_DB.label, Costanti.PSW_DB.label);
			String query = "select codice from csvjson where citta=?"; // QUERY VALIDA PER MYSQL
			PreparedStatement ps = con.prepareStatement(query);
			ps.setString(1, comune);
			ResultSet rs = ps.executeQuery();
			if (rs.next())
				codice = rs.getString(1);
			else
				throw new SQLException();
		} catch (SQLException ex) {
			System.out.printf(Costanti.MESSAGGIO_ERRORE_COLLEGAMENTO.label, Costanti.DATABASE.label,
					ex.getLocalizedMessage());
			System.out.printf(Costanti.MESSAGGIO_ATTENZIONE_ALTERNATIVA.label, Costanti.DATABASE.label,
					Costanti.JSON.label);
			while (true) {
				File f = new File(Costanti.PERCORSO_JSON.label);
				if (f.exists() && f.isFile() && f.canRead()) {
					codici = om.readValue(f, new TypeReference<List<CodiceCatastale>>() {
					});
					HashMap<String,String> mappa = listToHashmap(codici);
					if(mappa.containsKey(comune.toLowerCase())) {
						codice=mappa.get(comune.toLowerCase());
						break;
					}else {
						continue;
					}
				}
				System.out.printf(Costanti.MESSAGGIO_ERRORE_COLLEGAMENTO.label, Costanti.JSON.label,
						Costanti.MOTIVAZIONE_FILE.label);
				System.out.printf(Costanti.MESSAGGIO_ATTENZIONE_ALTERNATIVA.label, Costanti.JSON.label,
						Costanti.XML.label);
				f = new File(Costanti.PERCORSO_XML.label);
				if (f.exists() && f.isFile() && f.canRead()) {
					JAXBContext jaxbContext = JAXBContext.newInstance(ListaCodiciCatastali.class);
					Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
					codici = ((ListaCodiciCatastali) jaxbUnmarshaller.unmarshal(f)).getRow();
					HashMap<String,String> mappa = listToHashmap(codici);
					if(mappa.containsKey(comune.toLowerCase())) {
						codice=mappa.get(comune.toLowerCase());
						break;
					}else {
						continue;
					}
				}
				System.out.printf(Costanti.MESSAGGIO_ERRORE_COLLEGAMENTO.label, Costanti.XML.label,
						Costanti.MOTIVAZIONE_FILE.label);
				System.out.println("USCITA IN CORSO...");
				System.exit(-1);
			}
		}
		
		CarattereDiControllo cdc = new CarattereDiControllo(nominativo, formDataNascita, codice);
		String codiceFiscale=new StringBuilder(nominativo).append(formDataNascita).append(codice).append(cdc.getCarattere()).toString();
		System.out.printf(Costanti.STAMPA_RISULTATO.label,codiceFiscale);
	}
	
	private static HashMap<String,String> listToHashmap(List<CodiceCatastale> codici){
		HashMap<String,String> mappa = new HashMap<String, String>();
		for(CodiceCatastale cc : codici) {
			mappa.put(cc.getComune().toLowerCase(), cc.getCodice());
		}
		return mappa;
	}
}
